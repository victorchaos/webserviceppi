package cc.ppi.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import cc.ppi.jdbc.dao.AluguelDao;
import cc.ppi.jdbc.dao.CarroDao;
import cc.ppi.rest.entities.Carro;

@Path ("/carro")
public class RestCarro {	
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response getCarro(@QueryParam("renavan") String renavan, @QueryParam("format") String format, @QueryParam("categoria") String categoriaStr ){
		// verifica se a o retorno ser� em json ou xml, caso n�o defina no format= por padr�o ir� em json
		MediaType mediaType = null;
		if (format != null) {
			if (format.equals("json"))
				mediaType = MediaType.valueOf(MediaType.APPLICATION_JSON);
			else 
				mediaType = MediaType.valueOf(MediaType.APPLICATION_XML);
		}
		else
			mediaType = MediaType.valueOf(MediaType.APPLICATION_JSON);		
		CarroDao dao = new CarroDao();
		AluguelDao daoa = new AluguelDao();
		int categoria = -1;
		//vefirica se a categoria passada � v�lida e transforma em ID
		if (categoriaStr != null) {
			switch (categoriaStr.toLowerCase()) {
			case "suv":
				categoria = 2;
				break;
			case "exec":
				categoria = 6;
				break;
			case "econ":
				categoria = 1;
				break;
			case "intermed":
				categoria = 5;
				break;
				default:
					categoria = -1;
					break;
			}
		}
		// Verificar se o renavan foi passado
		if (renavan == null) {
			// se a categoria � v�lida retorna a lista com carros daquela categoria
			if (categoria != -1) {
				GenericEntity<List<Carro>> entity = new GenericEntity<List<Carro>>(dao.getListaByCatego(Integer.valueOf(categoria))) {};
				return Response				
			            .ok(entity)	            
			            .header(HttpHeaders.CONTENT_TYPE, mediaType)
			            .build();
			// se a categoria n�o existir, retorna a lista com todos os carros
			}else {
				GenericEntity<List<Carro>> entity = new GenericEntity<List<Carro>>(dao.getLista()) {};
				return Response				
			            .ok(entity)	            
			            .header(HttpHeaders.CONTENT_TYPE, mediaType)
			            .build();
			}
		}
		// se o renavan for "available" retorna a lista com todas os carros que n est�o alugados
		else if (renavan.equals("available")) {
			List<Carro> lista = new ArrayList<Carro>();
			for (Carro car : dao.getLista()) {
				if (daoa.getLista(car.getRenavan()).size() == 0)
					lista.add(car);
			}
			GenericEntity<List<Carro>> entity = new GenericEntity<List<Carro>>(lista) {};
			return Response				
		            .ok(entity)	            
		            .header(HttpHeaders.CONTENT_TYPE, mediaType)
		            .build();		
		}
		// se o renavan for "v�lido" tenta pegar o carro do banco de dados
		else {
			try {
				// se existir carro com aquele renavan no banco ele retorna o carro
				if (dao.getCarro(renavan).getNome() != null)
					return Response				
				            .ok(dao.getCarro(renavan))	            
				            .header(HttpHeaders.CONTENT_TYPE, mediaType)
				            .build();
				// se n existir ele retorna erro
				else Response				
	            .ok("N�o encontrado no sistema")	            
	            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML)
	            .build();;
			}catch (Exception e) {
				Response				
	            .ok("N�o encontrado no sistema")	            
	            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML)
	            .build();
			}			
		}
		return Response				
	            .ok("N�o encontrado no sistema")	            
	            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML)
	            .build();		
	}
	
	@DELETE
	@Produces(MediaType.APPLICATION_XML)
	public Response deleteCarro(@QueryParam("renavan") String renavan){
		// verifica se foi passado renavan no parametro
		if (renavan != null){
			CarroDao dao = new CarroDao();
			// se foi passado renavan no parametro ele tenta remover o carro do banco, se remover com sucesso ele retorna a mensagem com o renavan
			if (!dao.remove(renavan)) {
				return Response				
			            .ok("Carro com renavan = " + renavan + " excluido")	            
			            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML)
			            .build();
			}
		}
		// se n�o ele retorna o erro
		return Response				
	            .ok("N�o encontrado no sistema")	            
	            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML)
	            .build();
		
	}
	
	@PUT
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response updateCarro(Carro carro, @HeaderParam("Accept") String accepted) {
		CarroDao dao = new CarroDao();		
		if (!dao.altera(carro)){
			return Response				
	            .ok(dao.getCarro(carro.getRenavan()))	            
	            .header(HttpHeaders.CONTENT_TYPE, MediaType.valueOf(accepted))
	            .build();	
		}
		return Response				
	            .ok("Carro n�o encontrado no sistema")	            
	            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML)
	            .build();
	}
	
}
