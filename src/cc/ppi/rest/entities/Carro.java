package cc.ppi.rest.entities;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Carro {
	private String renavan;
	private String nome;
	private Categoria categoria;
	private int ano;
	
	public Carro() {
		
	}

	public String getRenavan() {
		return renavan;
	}

	public void setRenavan(String renavan) {
		this.renavan = renavan;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}
	
}
