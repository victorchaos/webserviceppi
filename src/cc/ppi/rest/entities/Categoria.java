package cc.ppi.rest.entities;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Categoria {
	private String nome;
	private int id;
	private double valor;
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public Categoria() {
		
	}
	
	
}
