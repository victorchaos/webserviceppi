package cc.ppi.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import cc.ppi.jdbc.dao.AluguelDao;
import cc.ppi.jdbc.dao.ClienteDao;
import cc.ppi.rest.entities.Aluguel;
import cc.ppi.rest.entities.Cliente;

@Path("/aluguel")
public class RestAluguel {
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response getCliente(@QueryParam("id") String id, @QueryParam("format") String format){
		// verifica se a o retorno ser� em json ou xml, caso n�o defina no format= por padr�o ir� em json
		MediaType mediaType = null;
		if (format != null) {
			if (format.equals("json"))
				mediaType = MediaType.valueOf(MediaType.APPLICATION_JSON);
			else 
				mediaType = MediaType.valueOf(MediaType.APPLICATION_XML);
		}
		else
			mediaType = MediaType.valueOf(MediaType.APPLICATION_JSON);		
		AluguelDao dao = new AluguelDao();		
		// Verificar se o id foi passado
		if (id == null) {
			// se id n for passado retorna a lista com todos os alugueis
			GenericEntity<List<Aluguel>> entity = new GenericEntity<List<Aluguel>>(dao.getLista()) {};
			return Response				
			        .ok(entity)	            
			        .header(HttpHeaders.CONTENT_TYPE, mediaType)
			        .build();			
		}
		// se o id for "v�lido" tenta pegar o aluguel do banco de dados
		else {
			try {
				// se existir aluguel com aquele id no banco ele retorna o carro
				if (dao.getAluguel(Integer.valueOf(id)).getCliente().getNome() != null)
					return Response				
				            .ok(dao.getAluguel(Integer.valueOf(id)))	            
				            .header(HttpHeaders.CONTENT_TYPE, mediaType)
				            .build();
				// se n existir ele retorna erro
				else Response				
	            .ok("N�o encontrado no sistema")	            
	            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML)
	            .build();;
			}catch (Exception e) {
				Response				
	            .ok("N�o encontrado no sistema")	            
	            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML)
	            .build();
			}			
		}
		return null;	
	}

}
