package cc.ppi.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import cc.ppi.jdbc.dao.ClienteDao;
import cc.ppi.rest.entities.Cliente;

@Path("/cliente")
public class RestCliente {
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response getCliente(@QueryParam("cpf") String cpf, @QueryParam("format") String format){
		// verifica se a o retorno ser� em json ou xml, caso n�o defina no format= por padr�o ir� em json
		MediaType mediaType = null;
		if (format != null) {
			if (format.equals("json"))
				mediaType = MediaType.valueOf(MediaType.APPLICATION_JSON);
			else 
				mediaType = MediaType.valueOf(MediaType.APPLICATION_XML);
		}
		else
			mediaType = MediaType.valueOf(MediaType.APPLICATION_JSON);		
		ClienteDao dao = new ClienteDao();		
		// Verificar se o cpf foi passado
		if (cpf == null) {
			// se cpf n for passado retorna a lista com todos os clientes
			GenericEntity<List<Cliente>> entity = new GenericEntity<List<Cliente>>(dao.getLista()) {};
			return Response				
			        .ok(entity)	            
			        .header(HttpHeaders.CONTENT_TYPE, mediaType)
			        .build();			
		}
		// se o cpf for "v�lido" tenta pegar o carro do banco de dados
		else {
			try {
				// se existir cliente com aquele cpf no banco ele retorna o carro
				if (dao.getCliente(cpf).getNome() != null)
					return Response				
				            .ok(dao.getCliente(cpf))	            
				            .header(HttpHeaders.CONTENT_TYPE, mediaType)
				            .build();
				// se n existir ele retorna erro
				else Response				
	            .ok("N�o encontrado no sistema")	            
	            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML)
	            .build();;
			}catch (Exception e) {
				Response				
	            .ok("N�o encontrado no sistema")	            
	            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML)
	            .build();
			}			
		}
		return null;	
	}
}
