package cc.ppi.jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cc.ppi.jdbc.ConnectionFactory;
import cc.ppi.rest.entities.Cliente;

public class ClienteDao {
	// a conex�o com o banco de dados
	private Connection connection;
	
	public ClienteDao() {
		this.connection = new ConnectionFactory().getConnection();
	}
	
	public ClienteDao(Connection c) {
		this.connection = c;
	}
	
	// m�todos do DAO
	
	public boolean adiciona(Cliente dado) {
		
		String sql = "insert into cliente " +
		"(cpf, nome, email, endereco, nascimento, telefone, senha)" +
		" values (?,?,?,?,?,?,?)";
		
		try {
			// prepared statement para inserção
			PreparedStatement stmt = connection.prepareStatement(sql);
		
			// seta os valores
			stmt.setString(1, dado.getCpf());
			stmt.setString(2, dado.getNome());
			stmt.setString(3, dado.getEmail());
			stmt.setString(4, dado.getEndere�o());
			stmt.setObject(5, dado.getNascimento());
			stmt.setString(6, dado.getTelefone());
			stmt.setString(7, dado.getSenha());
		
			// executa
			stmt.execute();
			stmt.close();
			return true;
		} catch (SQLException e) {
			return false;
		}
		
	}	

	public List<Cliente> getLista() {
		
		try {
			List<Cliente> dado = new ArrayList<Cliente>();
			PreparedStatement stmt = this.connection.prepareStatement("select * from cliente");
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				// criando o objeto Contato
				Cliente cliente = new Cliente();
				cliente.setCpf(rs.getString("cpf"));
				cliente.setNascimento(rs.getString("nascimento"));
				cliente.setEmail(rs.getString("email"));
				cliente.setNome(rs.getString("nome"));
				cliente.setTelefone(rs.getString("telefone"));
				cliente.setSenha(rs.getString("senha"));
				cliente.setEndere�o(rs.getString("endereco"));
		
				// adicionando o objeto � lista
				dado.add(cliente);
		
			}
		
			rs.close();
			stmt.close();
		
			return dado;
		
		} catch (SQLException e) {
		throw new RuntimeException(e);
		}
		
	}
	
	public void altera(Cliente dado) {
		String sql = "update cliente set nome = ?, email = ?, endereco = ?, nascimento = ?, telefone = ? where cpf = ?";
		try {
			
			PreparedStatement stmt = connection.prepareStatement(sql);
			// seta os valores
			stmt.setString(6, dado.getCpf());
			stmt.setString(1, dado.getNome());
			stmt.setString(2, dado.getEmail());
			stmt.setString(3, dado.getEndere�o());
			stmt.setObject(4, dado.getNascimento());
			stmt.setString(5, dado.getTelefone());
			
			//erro aqui
			stmt.execute();				
			stmt.close();
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
	}
	
	public void remove(String string) {
		
		try {
			
			PreparedStatement stmt = connection.prepareStatement("delete from cliente where cpf = ?");
			stmt.setString(1, string);
			stmt.execute();
			stmt.close();
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public Cliente getCliente(String cpf) {
		
		try {
			PreparedStatement stmt = this.connection.prepareStatement("select * from cliente where cpf = " + "'" + cpf + "'");
			ResultSet rs = stmt.executeQuery();
			Cliente cliente = new Cliente();
			while (rs.next()) {
				// criando o objeto Contato
				
				cliente.setNascimento(rs.getString("nascimento"));
				cliente.setEmail(rs.getString("email"));
				cliente.setNome(rs.getString("nome"));
				cliente.setTelefone(rs.getString("telefone"));
				cliente.setSenha(rs.getString("senha"));
				cliente.setCpf(rs.getString("cpf"));
				cliente.setEndere�o(rs.getString("endereco"));
		
			}
		
			rs.close();
			stmt.close();
		
			return cliente;
		
		} catch (SQLException e) {
		throw new RuntimeException(e);
		}
		
	}
}
