package cc.ppi.jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cc.ppi.jdbc.ConnectionFactory;
import cc.ppi.rest.entities.Aluguel;

public class AluguelDao {
		// a conex�o com o banco de dados
		private Connection connection;
		
		public AluguelDao() {
			this.connection = new ConnectionFactory().getConnection();
		}
		
		public AluguelDao(Connection c) {
			this.connection = c;
		}
		
		// m�todos do DAO
		
		public int adiciona(Aluguel dado) {
			
			String sql = "insert into aluguel " +
			"(cpf_cliente, renavan_carro, data_inicio, data_fim, valor_estimado, valor_total, id)" +
			" values (?,?,?,?,?,?,?)";
			int id = getNextId();
			try {
				// prepared statement para inserção
				PreparedStatement stmt = connection.prepareStatement(sql);
			
				// seta os valores
				//stmt.setString(1, dado.getCliente().getCpf());
				//stmt.setString(2, dado.getCarro().getRenavan());
				stmt.setObject(3, dado.getData_inicio());
				stmt.setObject(4, dado.getData_fim());
				//stmt.setDouble(5, dado.getValor_estimado());
				//stmt.setDouble(6, dado.getValor_total());
				stmt.setInt(7, id);				
				
				// executa
				stmt.execute();
				stmt.close();
				
				return id;
			} catch (SQLException e) {
			throw new RuntimeException(e);
			}
			
		}	

		private int getNextId() {
			try {				
				PreparedStatement stmt = this.connection.prepareStatement("select max(id) as id from aluguel");
				ResultSet rs = stmt.executeQuery();
				int nextId = 0;
				while (rs.next()) {
					nextId = rs.getInt("id");
			
				}
			
				rs.close();
				stmt.close();
			
				return nextId+1;
			
			} catch (SQLException e) {
			throw new RuntimeException(e);
			}
		}

		public List<Aluguel> getLista() {
			
			try {
				List<Aluguel> dado = new ArrayList<Aluguel>();
				PreparedStatement stmt = this.connection.prepareStatement("select * from aluguel");
				ResultSet rs = stmt.executeQuery();
				while (rs.next()) {
					// criando o objeto Contato
					Aluguel aluguel = new Aluguel();
					ClienteDao daocl = new ClienteDao();					
					aluguel.setCliente(daocl.getCliente(rs.getString("cpf_cliente")));
					CarroDao daoc = new CarroDao();
					aluguel.setCarro(daoc.getCarro(rs.getString("renavan_carro")));
					aluguel.setData_inicio(rs.getString("data_inicio"));
					aluguel.setData_fim(rs.getString("data_fim"));
					aluguel.setId(rs.getInt("id"));
					aluguel.setValor_estimado(rs.getDouble("valor_estimado"));
					aluguel.setValor_total(rs.getDouble("valor_total"));
					// adicionando o objeto � lista
					dado.add(aluguel);
			
				}
			
				rs.close();
				stmt.close();
			
				return dado;
			
			} catch (SQLException e) {
			throw new RuntimeException(e);
			}
			
		}
		
		public List<Aluguel> getLista(String renavan_carro) {
			
			try {
				List<Aluguel> dado = new ArrayList<Aluguel>();
				PreparedStatement stmt = this.connection.prepareStatement("select * from aluguel where renavan_carro = " + renavan_carro);
				ResultSet rs = stmt.executeQuery();
				while (rs.next()) {
					// criando o objeto Contato
					Aluguel aluguel = new Aluguel();
					ClienteDao daocl = new ClienteDao();					
					aluguel.setCliente(daocl.getCliente(rs.getString("cpf_cliente")));
					CarroDao daoc = new CarroDao();
					aluguel.setCarro(daoc.getCarro(rs.getString("renavan_carro")));
					aluguel.setData_inicio(rs.getString("data_inicio"));
					aluguel.setData_fim(rs.getString("data_fim"));
					aluguel.setId(rs.getInt("id"));
					aluguel.setValor_estimado(rs.getDouble("valor_estimado"));
					aluguel.setValor_total(rs.getDouble("valor_total"));
					// adicionando o objeto � lista
					dado.add(aluguel);
			
				}
			
				rs.close();
				stmt.close();
			
				return dado;
			
			} catch (SQLException e) {
			throw new RuntimeException(e);
			}
			
		}
		
		public void altera(Aluguel dado) {
			String sql = "update aluguel set cpf_cliente = ?, renavan_carro = ?, data_inicio = ?, data_fim = ?, valor_estimado = ?, valor_total = ? where id = ?";
			try {
				
				PreparedStatement stmt = connection.prepareStatement(sql);
				stmt.setString(1, dado.getCliente().getCpf());
				stmt.setString(2, dado.getCarro().getRenavan());
				stmt.setObject(3, dado.getData_inicio());
				stmt.setObject(4, dado.getData_fim().toString());
				stmt.setDouble(5, dado.getValor_estimado());
				stmt.setDouble(6, dado.getValor_total());
				stmt.setInt(7, dado.getId());
			
				stmt.execute();
				stmt.close();
				
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
			
		}
		
		public void remove(String string) {
			
			try {
				
				PreparedStatement stmt = connection.prepareStatement("delete from aluguel where id = ?");
				stmt.setInt(1, Integer.parseInt(string));
				stmt.execute();
				stmt.close();
				
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
		
		public Aluguel getAluguel(int id) throws SQLException {
			
			try {
				Aluguel aluguel = new Aluguel();
				PreparedStatement stmt = this.connection.prepareStatement("select * from aluguel where id = " + id + ";");
				ResultSet rs = stmt.executeQuery();
				while (rs.next()) {
					// criando o objeto Contato					
					ClienteDao daocl = new ClienteDao();					
					aluguel.setCliente(daocl.getCliente(rs.getString("cpf_cliente")));
					CarroDao daoc = new CarroDao();
					aluguel.setCarro(daoc.getCarro(rs.getString("renavan_carro")));
					aluguel.setData_inicio(rs.getString("data_inicio"));
					aluguel.setData_fim(rs.getString("data_fim"));
					aluguel.setId(rs.getInt("id"));
					aluguel.setValor_estimado(rs.getDouble("valor_estimado"));
					aluguel.setValor_total(rs.getDouble("valor_total"));
					
				}
			
				rs.close();
				stmt.close();
			
				return aluguel;
			
			} catch (SQLException e) {
			throw new SQLException(e);
			}
			
		}
		
}
